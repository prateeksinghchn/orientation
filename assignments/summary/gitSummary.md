The Orientation Module gave an idea of the entire workflow.
It was told that learning a skill is divided into 3 stages(USP) :-

Stage 1- Understand (U)
Stage 2- Summarize (S)
Stage 3- Practice (P)

The learning material will be available in the "Wiki" section of GitLab.
The assingnments and any questions related to the same may be posted in the Issues section.  